package definition;

import core.Driver;
import core.Log;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import static java.lang.Thread.sleep;

public class Stepdefs {

    @Given("^I open the chrome browser and go to jenkins.io homepage$")
    public void whenIAmOnPage() throws InterruptedException {
        Driver.get().get("https://jenkins.io/");
        sleep(5000);
    }

    @When("^I click on documentation button$")
    public void whenIClickOnDocumenationButton() throws InterruptedException {
        Driver.get().findElement(By.xpath("//a[@class='btn btn-secondary m-1']")).click();
        sleep(5000);
    }

    @Then("^Title is visible$")
    public void thenTitleShouldBeVisible() {
        Log.verifyTrue("Title is visible", Driver.get().findElement(By.xpath("//h1")).isDisplayed());
    }

    @And("^I close the chrome browser$")
    public void closeChromeBrowser() {
        Driver.get().quit();
    }
}
