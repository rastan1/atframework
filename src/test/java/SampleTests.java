import core.Base;
import core.Element;
import core.Log;
import core.Page;
import org.junit.Test;

public class SampleTests extends Base {

    @Test
    public void testElementsOnJenkinsPage() {
        Page p = new Page("https://jenkins.io/");
        Element documentationBtn = new Element("//a[@class='btn btn-secondary m-1']");
        Log.verifyTrue("Documentation button is visible", documentationBtn.isVisible());
        documentationBtn.click();
        Element title = new Element("//h1");
        Log.verifyTrue("Title is visible", title.isVisible());
        Log.verifyTrue("Title text is correct", title.getText().equals("Jenkins User Documentation"));
    }


    @Test
    public void testElementsOnStackoverflowPage() {
        Page p = new Page("https://stackoverflow.com/");
        Element searchBar = new Element("//form[@id='search']");
        Log.verifyTrue("Search bar is visible", searchBar.isVisible());
    }


}
