package core;

import junit.framework.TestResult;

public class Page extends Base {

    public static TestResult PassFailMonitor    = null;

    public Page(String url) {
        Driver.get().get(url);
    }

    public void close() {
        Driver.get().close();
    }
}
