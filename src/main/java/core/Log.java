package core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Log {

    public static void verifyTrue(String message, boolean condition) {
        System.out.println("Checking " + message);
        assertTrue(message, condition);
    }

    public static void verifyFalse(String message, boolean condition) {
        System.out.println("Checking " + message);
        assertFalse(message, condition);
    }
}
