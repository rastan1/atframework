package core;

import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.PrintWriter;
import java.io.StringWriter;

import static core.Base.logger;
import static java.sql.DriverManager.getDriver;
import static junit.framework.TestCase.fail;

public class Element {
    private String locator;

    private TestName name = new TestName();

    private String getTestName(){
        return name.getMethodName();
    }

    private Wait wait;

    public Element(String locator) {
        this.locator = locator;
        this.wait = new Wait();
    }

    private WebElement getWebElement() {
        return Base.getDriver().findElement(findLocator(locator));
    }


    public void click(Boolean... assertOnFail) {

        assert assertOnFail.length <= 1;
        boolean assertParam = assertOnFail.length <= 0 || assertOnFail[0].booleanValue();

        try {
            wait.toBeVisible(10);
            getWebElement().click();
            logger.debug("Command executed:  click(" + locator + ")");
        } catch (Exception e) {
            logger.debug("Exception when trying to execute command:  click(" + locator + "): " + "\n" + e);
            Page.PassFailMonitor.addError(null, e);
            if (assertParam) {
                fail(throwableToString(e));

            }
        }

    }

    public String getText() {
        String text = null;

        try {
            wait.toBeVisible(10);
            text = getWebElement().getText();
            logger.debug("Command executed:  getText(" + locator + ")");
        } catch (Exception e) {
            logger.debug("Exception when trying to execute command:  click(" + locator + "): " + "\n" + e);
        }
        return text;
    }

    public boolean isVisible() {
        logger.debug("Checking isVisible(" + locator + ")");

        try {
            return getWebElement().isDisplayed();
        } catch (Exception e) {
            logger.debug("Exception when trying to execute command isVisible(" + locator + "): " + "\n" +  e);
            Page.PassFailMonitor.addError(null, e);
            return false;
        }

    }

    private By findLocator(String s) {
        LocatorType strg1choise = LocatorType.byId;
        LocatorType strg2choise = LocatorType.byName;
        LocatorType strg3choise = LocatorType.byCss;

        Boolean path = s.startsWith("/");
        Boolean path1 = s.startsWith("(/");
        Boolean link = s.startsWith("link=");
        Boolean partiallink = s.startsWith("partiallink=");
        Boolean classname = s.startsWith("classname=");
        Boolean tagname = s.startsWith("tagname=");

        if (path || path1) {
            System.out.println("Found element [" + locator + "] by xpath");
            return new By.ByXPath(s);
        } else if (link) {
            System.out.println("Found element [" + locator + "] by link");
            return new By.ByLinkText(s.substring(5, s.length()));
        } else if (partiallink) {
            System.out.println("Found element [" + locator + "] by partiallink");
            return new By.ByPartialLinkText(s.substring(12, s.length()));
        } else if (classname) {
            System.out.println("Found element [" + locator + "] by classname");
            return new By.ByClassName(s.substring(10, s.length()));
        } else if (tagname) {
            System.out.println("Found element [" + locator + "] by tagname");
            return new By.ByTagName(s.substring(8, s.length()));
        } else if (isElement(strg1choise, s)) {
            return getLocator(strg1choise, s);
        } else if (isElement(strg2choise, s)) {
            return getLocator(strg2choise, s);
        } else if (isElement(strg3choise, s)) {
            return getLocator(strg3choise, s);
        }
        {
            System.out.println("Could not find element [" + locator + "]");
            throw new IllegalArgumentException("Element identified by: " + locator + ", not found");
        }

    }

    private enum LocatorType {

        byId, byName, byCss
    }

    private static By getLocator(LocatorType selection, String s) {
        switch (selection) {
            case byId:
                return new By.ById(s);
            case byName:
                return new By.ByName(s);
            case byCss:
                return new By.ByCssSelector(s);
        }
        return null;
    }

    private boolean isElement(LocatorType selection, String s) {
        switch (selection) {
            case byId:
                if (!Base.getDriver().findElements(By.id(s)).isEmpty()) { // check if the string is an id locator
                    System.out.println("Found element [" + locator + "] by id");
                    return true;
                }
                break;
            case byName:
                if (!Base.getDriver().findElements(By.name(s)).isEmpty()) { // check if the string is a name locator
                    System.out.println("Found element [" + locator + "] by name");
                    return true;
                }
                break;
            case byCss:
                if (!Base.getDriver().findElements(By.cssSelector(s)).isEmpty()) { // check if the string is a Css locator
                    System.out.println("Found element [" + locator + "] by css");
                    return true;
                }
                break;
        }
        return false;
    }

    private String throwableToString(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }

    public class Wait {

        public void toBeVisible(int timeout, Boolean... assertOnFail) {
            assert assertOnFail.length <= 1;
            boolean logFail = assertOnFail.length > 0 ? assertOnFail[0] : true;

            try {
                new WebDriverWait(Base.getDriver(), timeout).until(ExpectedConditions.visibilityOfElementLocated(findLocator(locator)));
            } catch (TimeoutException e) {
                logger.debug("Timed out waiting for '" + locator + "' to become visible");
                if (logFail) {
                    fail("Timeout after '" + timeout + "' seconds in test " + getTestName());
                }
            } catch (Exception e) {
                Page.PassFailMonitor.addError(null, e);
                logger.debug("Exception when trying to execute command  waitForElementToBecomeVisible(" + locator + "," + timeout + "): " + "\n" + e);
            }
        }


    }


}
