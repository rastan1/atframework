package core;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class Base extends TestCase {

    private static WebDriver driver;

    public static org.apache.log4j.Logger logger;

    public static WebDriver getDriver() {
        return driver;
    }

    @Before
    public void setUp() throws Exception {
        driver = Driver.get();
        logger = org.apache.log4j.Logger.getLogger("DEBUG");
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
