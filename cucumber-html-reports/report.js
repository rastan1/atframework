$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Cucumber.feature");
formatter.feature({
  "line": 2,
  "name": "Cucumber tests",
  "description": "",
  "id": "cucumber-tests",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tests"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Jenkins.io test case",
  "description": "",
  "id": "cucumber-tests;jenkins.io-test-case",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I open the chrome browser and go to jenkins.io homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on documentation button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Title is visible",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I close the chrome browser",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefs.whenIAmOnPage()"
});
formatter.result({
  "duration": 11743309117,
  "status": "passed"
});
formatter.match({
  "location": "Stepdefs.whenIClickOnDocumenationButton()"
});
formatter.result({
  "duration": 6675492555,
  "status": "passed"
});
formatter.match({
  "location": "Stepdefs.thenTitleShouldBeVisible()"
});
formatter.result({
  "duration": 42427752,
  "status": "passed"
});
formatter.match({
  "location": "Stepdefs.closeChromeBrowser()"
});
formatter.result({
  "duration": 1771982836,
  "status": "passed"
});
});